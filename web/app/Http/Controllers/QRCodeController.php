<?php

namespace App\Http\Controllers;

use App\Exceptions\ShopifyProductCreatorException;
use App\Http\Requests\StoreQRCodeRequest;
use App\Http\Requests\UpdateQRCodeRequest;
use App\Models\QRCode;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Shopify\Rest\Admin2023_04\Product;

class QRCodeController extends Controller
{
    public function index(Request $request)
    {
        $session = $request->get('shopifySession');

        $items = QRCode::all();
        foreach ($items as $item) {
            $productId = $item->productId;
            $product = Product::find($session, $productId);
            $item->product = $product;
        }
        return $items;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreQRCodeRequest $request
     * @return JsonResponse
     */
    public function store(StoreQRCodeRequest $request)
    {
        $item = [];
        try {
            $data = $request->post();
            $data['productId'] = $this->splitId($data['productId']);
            $data['variantId'] = $this->splitId($data['variantId']);

            $data['shopDomain'] = 'shopDomain';
            $data['discountId'] = 'discountId';
            $data['discountCode'] = 'discountCode';
            $data['scans'] = 'scans';

            $item = QRCode::create($data);
            $success = true;
            $code = 200;
            $error = null;
        } catch (Exception $e) {
            $success = false;

            if ($e instanceof ShopifyProductCreatorException) {
                $code = $e->response->getStatusCode();
                $error = $e->response->getDecodedBody();
                if (array_key_exists("errors", $error)) {
                    $error = $error["errors"];
                }
            } else {
                $code = 500;
                $error = $e->getMessage();
            }

            Log::error("Failed to create products: $error");
        } finally {
            return response()->json(["success" => $success, "error" => $error, 'data' => $item], $code);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param string $id
     * @return Response
     */
    public function show(string $id, Request $request)
    {
        $session = $request->get('shopifySession');
        $item = QRCode::find($id);
        $productId = $item->productId;
        $product = Product::find($session, $productId);
        $item->product = $product;
        return $item;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateQRCodeRequest $request
     * @param QRCode $qRCode
     * @return Response
     */
    public function update(UpdateQRCodeRequest $request, QRCode $qRCode)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param string $id
     * @return JsonResponse
     */
    public function destroy(string $id): JsonResponse
    {
        $item = QRCode::find($id);
        $result = $item->delete();
        return response()->json(['data' => $result], 200);
    }

    private function splitId(string $string): string
    {
        $string = explode("/", $string);
        $id = end($string);
        return $id;
    }
}
