<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQRCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('q_r_codes', function (Blueprint $table) {
            $table->id();
            $table->string('shopDomain', 511);
            $table->string('title', 511)->nullable(false);
            $table->string('productId', 255)->nullable(false);
            $table->string('variantId', 255)->nullable(false);
            $table->string('handle', 255)->nullable(false);
            $table->string('discountId', 255)->nullable(false);
            $table->string('discountCode', 255)->nullable(false);
            $table->string('destination', 255)->nullable(false);
            $table->integer('scans');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('q_r_codes');
    }
}
