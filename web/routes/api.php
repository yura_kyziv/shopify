<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', function () {
    return "Hello API";
});

Route::get('orders', function (Request $request) {
    /** @var AuthSession */
    $session = $request->get('shopifySession'); // Provided by the shopify.auth middleware, guaranteed to be active

    $result = \Shopify\Rest\Admin2023_04\Order::all($session);

    return $result;
})->middleware('shopify.auth');

Route::get('qrcodes', [\App\Http\Controllers\QRCodeController::class, 'index'])->middleware('shopify.auth');
Route::get('qrcodes/{id}', [\App\Http\Controllers\QRCodeController::class, 'show'])->middleware('shopify.auth');
Route::delete('qrcodes/{id}', [\App\Http\Controllers\QRCodeController::class, 'destroy'])->middleware('shopify.auth');
Route::post('qrcodes', [\App\Http\Controllers\QRCodeController::class, 'store'])->middleware('shopify.auth');
