import {
  Card,
  Page,
  Layout,
  TextContainer,
  Image,
  Stack,
  Link,
  Heading, Tag,
} from "@shopify/polaris";
import { TitleBar } from "@shopify/app-bridge-react";

import { trophyImage } from "../assets";

import { OrdersTable } from "../components";
import { Tab } from "../components";
import {Route, Routes} from "react-router-dom";

export default function HomePage() {
  return (
    <Page narrowWidth>
      <TitleBar title="App name" primaryAction={null} />
      <Layout>
        <Layout.Section fullWidth>
          <Tab />
        </Layout.Section>
        <Layout.Section>
          <OrdersTable />
        </Layout.Section>
      </Layout>
    </Page>
  );
}
