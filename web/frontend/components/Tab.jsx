import {LegacyCard, Tabs} from '@shopify/polaris';
import {useState, useCallback} from 'react';

export function Tab() {
    const [selected, setSelected] = useState(0);

    const handleTabChange = useCallback(
        (selectedTabIndex) => setSelected(selectedTabIndex),
        [],
    );

    const tabs = [
        {
            id: 'all-customers-fitted-2',
            content: 'All',
            accessibilityLabel: 'All customers',
            panelID: 'all-customers-fitted-content-2',
        },
        {
            id: 'accepts-marketing-fitted-2',
            content: 'Accepts marketing',
            panelID: 'accepts-marketing-fitted-Ccontent-2',
        },
        {
            id: 'accepts-marketing-fitted-21',
            content: 'Accepts marketing',
            panelID: 'accepts-marketing-fitted-Ccontent-21',
        },
        {
            id: 'accepts-marketing-fitted-22',
            content: 'Accepts marketing',
            panelID: 'accepts-marketing-fitted-Ccontent-22',
        },
        {
            id: 'accepts-marketing-fitted-23',
            content: 'Accepts marketing',
            panelID: 'accepts-marketing-fitted-Ccontent-23',
        },
        {
            id: 'accepts-marketing-fitted-24',
            content: 'Accepts marketing',
            panelID: 'accepts-marketing-fitted-Ccontent-24',
        },
        {
            id: 'accepts-marketing-fitted-25',
            content: 'Accepts marketing',
            panelID: 'accepts-marketing-fitted-Ccontent-25',
        },
        {
            id: 'accepts-marketing-fitted-26',
            content: 'Accepts marketing',
            panelID: 'accepts-marketing-fitted-Ccontent-26',
        },
        {
            id: 'accepts-marketing-fitted-27',
            content: 'Accepts marketing',
            panelID: 'accepts-marketing-fitted-Ccontent-27',
        },
    ];

    return (
        <LegacyCard>
            <Tabs tabs={tabs} selected={selected} onSelect={handleTabChange} fitted>
                <LegacyCard.Section title={tabs[selected].content}>
                    <p>Tab {selected} selected</p>
                </LegacyCard.Section>
            </Tabs>
        </LegacyCard>
    );
}