import {
    IndexTable,
    LegacyCard,
    useIndexResourceState,
    Text,
    Badge,
} from '@shopify/polaris';
import React from 'react';
import {useAppQuery} from "../hooks";

export function OrdersTable() {
    const orders = [
        {
            id: '1021',
            url: 'customers/123',
            name: 'Jaydon Stanton',
            total: '$969.44',
            location: 'Decatur, USA',
            orders: 20,
            amountSpend: "$2,400"
        },
        {
            id: '1020',
            url: 'customers/123',
            name: 'Jaydon Stanton',
            total: '$969.44',
            location: 'Decatur, USA',
            orders: 20,
            amountSpend: "$2,400"
        },
        {
            id: '1023',
            url: 'customers/123',
            name: 'Jaydon Stanton',
            total: '$969.44',
            location: 'Decatur, USA',
            orders: 20,
            amountSpend: "$2,400"
        },
    ];

    const { data, isLoading, isError, error } = useAppQuery({
        url: 'api/orders',
        reactQueryOptions: {
            select: (data) => {
                console.log(data)
                //requestData = data
                return data;
            },
            onSuccess: (response) => {
                //requestData = response
                console.log(response)
            }
        }
    });


    const resourceName = {
        singular: 'order',
        plural: 'orders',
    };
    console.log(data);
    const {selectedResources, allResourcesSelected, handleSelectionChange} =
        useIndexResourceState(data);
    console.log(data);

    const rowMarkup = data?.map(
        (
            {id, name, total_price, financial_status},
            index
        ) => (
            <IndexTable.Row
                id={id}
                key={id}
                selected={selectedResources.includes(id)}
                position={index}
            >
                <IndexTable.Cell>
                    <Text variant="bodyMd" fontWeight="bold" as="span">
                        {name}
                    </Text>
                </IndexTable.Cell>
                <IndexTable.Cell>{total_price}</IndexTable.Cell>
                <IndexTable.Cell>{financial_status}</IndexTable.Cell>
            </IndexTable.Row>
        ),
    );

    if (isLoading){
        console.log(data);
        return <h1>Loading...</h1>
    }

    if(isError){
        return <h1>error.message</h1>
    }

    return (
        <LegacyCard>
            <IndexTable
                resourceName={resourceName}
                itemCount={orders.length}
                selectedItemsCount={
                    allResourcesSelected ? 'All' : selectedResources.length
                }
                onSelectionChange={handleSelectionChange}
                headings={[
                    {title: '#'},
                    {title: 'Total Price'},
                    {title: 'Financial Status'}
                ]}
            >
                {rowMarkup}
            </IndexTable>
        </LegacyCard>
    );
}